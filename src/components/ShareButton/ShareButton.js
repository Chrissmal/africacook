import React from 'react';
import { TouchableHighlight, Image, Text, View } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

export default class ShareButton extends React.Component {
  render() {
    return (
      <TouchableHighlight underlayColor='light' style={styles.touchableHighlight} onPress={this.props.onPress}>
        <View style={styles.container}>
          <Text style={styles.text}>Share</Text>
        </View>
      </TouchableHighlight>
    );
  }
}

ShareButton.propTypes = {
  onPress: PropTypes.func,
  source: PropTypes.number,
  title: PropTypes.string
};