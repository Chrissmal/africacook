import { StyleSheet } from 'react-native';

const GLOBAL_COLOR = '#CCB803'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: 50,
    width: 270,
    marginTop: 20,
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 100,
    borderColor: GLOBAL_COLOR,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
   backgroundColor: GLOBAL_COLOR
  },
  touchableHighlight: {
    borderRadius: 100,
  },
  text: {
    fontSize: 14,
    color: '#fff'
  }
});

export default styles;
